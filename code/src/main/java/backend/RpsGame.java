package backend;
import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random random;

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        this.random = new Random();
    }

    public int getWins() {
        return this.wins;
    }

    public int getTies() {
        return this.ties;
    }

    public int getLosses() {
        return this.losses;
    }

    public String playRound(String choice) {
        int randomNumber = random.nextInt(3); 
        switch(randomNumber) {
            case 0: //Rock
                System.out.println("Computer: Rock");
                if(choice.equalsIgnoreCase("rock")) {
                    this.ties ++;
                    return "Tie";
                }else if(choice.equalsIgnoreCase("paper")) {
                    this.wins ++;
                    return "Win";
                }else {
                    this.losses ++;
                    return "Lose";
                }
            case 1: //Paper
                System.out.println("Computer: Paper");
                if(choice.equalsIgnoreCase("paper")) {
                    this.ties ++;
                    return "Tie";
                }else if(choice.equalsIgnoreCase("rock")) {
                    this.losses ++;
                    return "Lose";
                }else {
                    this.wins ++;
                    return "Win";
                }
            case 2: //scissors
                System.out.println("Computer: Scissors");
                if(choice.equalsIgnoreCase("scissors")) {
                    this.ties ++;
                    return "Tie";
                }else if(choice.equalsIgnoreCase("paper")) {
                    this.losses ++;
                    return "Lose";
                }else {
                    this.wins ++;
                    return "Win";
                }
        }
        return "";
    }
}
