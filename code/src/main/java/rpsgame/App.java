package rpsgame;
import java.util.Scanner;
import backend.*;
/**
 * Console application for rock paper scissors game
 *
 */
public class App {
    public static void main( String[] args) {
        //TODO
        boolean continueGame = true;
        Scanner sc = new Scanner(System.in);
        RpsGame game = new RpsGame();
        while(continueGame) {
            System.out.println("Enter your choice rock/paper/scissors or quit to exit");
            String choice = sc.next();
            sc.nextLine();
            if(choice.equalsIgnoreCase("quit")) {
                continueGame = false;
                continue;
            }
            System.out.println(game.playRound(choice)); 
            System.out.println("Number of wins: " + game.getWins());
            System.out.println("Number of losses: " + game.getLosses());
            System.out.println("Number of ties: " + game.getTies());
        }
    }
}
